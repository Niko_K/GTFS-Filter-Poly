package de.krismer.gtfs.transformers;

import com.google.common.base.Function;

import org.onebusaway.gtfs.model.StopTime;
import org.onebusaway.gtfs.model.Trip;

public final class StopTimeToTripFunction implements Function<StopTime, Trip> {
	@Override
	public Trip apply(final StopTime st) {
		return (st == null) ? null : st.getTrip();
	}
}
