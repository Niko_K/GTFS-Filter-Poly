package de.krismer.gtfs.transformers;

import com.google.common.base.Function;

import org.onebusaway.gtfs.model.Route;
import org.onebusaway.gtfs.model.Trip;

public final class TripToRouteFunction implements Function<Trip, Route> {
	@Override
	public Route apply(final Trip t) {
		return (t == null) ? null : t.getRoute();
	}
}
