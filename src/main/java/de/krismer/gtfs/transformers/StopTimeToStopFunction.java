package de.krismer.gtfs.transformers;

import com.google.common.base.Function;

import org.onebusaway.gtfs.model.Stop;
import org.onebusaway.gtfs.model.StopTime;

public final class StopTimeToStopFunction implements Function<StopTime, Stop> {
	@Override
	public Stop apply(final StopTime st) {
		return (st == null) ? null : st.getStop();
	}
}
