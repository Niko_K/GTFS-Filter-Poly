package de.krismer.gtfs.transformers;

import com.google.common.base.Function;

import org.onebusaway.gtfs.model.AgencyAndId;
import org.onebusaway.gtfs.model.ServiceCalendar;

public final class ServiceCalendarToServiceIdFunction implements Function<ServiceCalendar, AgencyAndId> {
	@Override
	public AgencyAndId apply(final ServiceCalendar sc) {
		return (sc == null) ? null : sc.getServiceId();
	}
}
