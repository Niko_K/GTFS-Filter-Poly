package de.krismer.gtfs.transformers;

import com.google.common.base.Function;

import org.onebusaway.gtfs.model.AgencyAndId;
import org.onebusaway.gtfs.model.ServiceCalendarDate;

public final class ServiceCalendarDateToServiceIdFunction implements Function<ServiceCalendarDate, AgencyAndId> {
	@Override
	public AgencyAndId apply(final ServiceCalendarDate scd) {
		return (scd == null) ? null : scd.getServiceId();
	}
}
