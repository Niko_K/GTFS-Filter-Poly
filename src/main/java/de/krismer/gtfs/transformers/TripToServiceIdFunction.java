package de.krismer.gtfs.transformers;

import com.google.common.base.Function;

import org.onebusaway.gtfs.model.AgencyAndId;
import org.onebusaway.gtfs.model.Trip;

public final class TripToServiceIdFunction implements Function<Trip, AgencyAndId> {
	@Override
	public AgencyAndId apply(final Trip t) {
		return (t == null) ? null : t.getServiceId();
	}
}
