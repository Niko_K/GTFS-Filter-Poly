package de.krismer.gtfs;

import com.google.common.collect.ImmutableMap;

import java.awt.Shape;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.locationtech.jts.awt.ShapeWriter;
import org.locationtech.jts.geom.Geometry;
import org.onebusaway.gtfs.impl.GtfsRelationalDaoImpl;
import org.onebusaway.gtfs.model.Agency;
import org.onebusaway.gtfs.model.Route;
import org.onebusaway.gtfs.model.ServiceCalendar;
import org.onebusaway.gtfs.model.ServiceCalendarDate;
import org.onebusaway.gtfs.model.Stop;
import org.onebusaway.gtfs.model.StopTime;
import org.onebusaway.gtfs.model.Trip;
import org.onebusaway.gtfs.model.calendar.ServiceDate;
import org.onebusaway.gtfs.serialization.GtfsReader;
import org.onebusaway.gtfs.serialization.GtfsWriter;
import org.onebusaway.gtfs.services.GtfsDao;
import org.onebusaway.gtfs.services.GtfsMutableRelationalDao;
import org.openstreetmap.osmosis.areafilter.common.PolygonFileReader;
import org.wololo.jts2geojson.GeoJSONReader;

import de.krismer.gtfs.filters.BoundingBoxDaoFilter;
import de.krismer.gtfs.filters.BoundingPolyDaoFilter;
import de.krismer.gtfs.filters.TimespanDaoFilter;
import de.krismer.gtfs.filters.TransportTypeDaoFilter;

public class FilterMain {
	private static final Logger LOG = LogManager.getLogger(FilterMain.class);

	// CLI constants
	private static final String DESCRIPTION_OPT_INPUT = "Input GTFS file/folder";
	private static final String DESCRIPTION_OPT_BOUNDING_BOX = "Remove locations outside the given bounding-box";
	private static final String DESCRIPTION_OPT_BOUNDING_POLY = "Filter locations outside the given poly file";
	private static final String DESCRIPTION_OPT_OUTPUT = "Output directory or GTFS archive file the filtered data will be stored in";
	private static final String DESCRIPTION_OPT_TIME = "Remove trips outside this timespan (format: yyyy-mm-dd)";
	private static final String DESCRIPTION_OPT_TYPE = "Only keeps trips with the given transport types. Possible values are: tram, subway, rail, bus, ferry, cablecar, gondola, funicular";

	private static final String BOUNDING_BOX_OPTION = "bb";
	private static final String BOUNDING_POLY_OPTION = "bp";
	private static final String INPUT_OPTION = "i";
	private static final String OUTPUT_OPTION = "o";
	private static final String TIME_OPTION = "d";
	private static final String TYPE_OPTION = "t";

	private static final String USAGE = "java -jar <pathToJar>/GTFS-Filter-Poly.jar";
	private static final String HEADER = "GTFS-Filter-Poly - Filters a GTFS-feed by a certain criteria (like a bounding box, a bounding polygon, by traveldate or by transporttype)";
	private static final String FOOTER = "";

	private static final ImmutableMap<String, Integer> TRANSPORT_TYPES;
	private static final String GTFS_EXTENSION = ".zip";

	static {
		// CHECKSTYLE:OFF MagicNumber
		TRANSPORT_TYPES = new ImmutableMap.Builder<String, Integer>()
			.put("tram", 0)
			.put("subway", 1)
			.put("rail", 2)
			.put("bus", 3)
			.put("ferry", 4)
			.put("cablecar", 5)
			.put("gondola", 6)
			.put("funicular", 7)
			.build();
		// CHECKSTYLE:ON MagicNumber
	}

	private final File input, output;

	private GtfsDao filteredDao = null;

	public FilterMain(final File input, final File output) {
		this.input = input;
		this.output = output;
	}

	public synchronized void read() {
		if (filteredDao != null) {
			throw new IllegalStateException("Reading has already finished");
		}
		final List<Class<?>> entityClasses = new ArrayList<>();
		entityClasses.add(Agency.class);
		entityClasses.add(Route.class);
		entityClasses.add(Stop.class);
		entityClasses.add(Trip.class);
		entityClasses.add(StopTime.class);
		entityClasses.add(ServiceCalendar.class);
		entityClasses.add(ServiceCalendarDate.class);

		final GtfsMutableRelationalDao dao = new GtfsRelationalDaoImpl();
		final GtfsReader gtfsReader = new GtfsReader();
		gtfsReader.setEntityStore(dao);
		gtfsReader.setEntityClasses(entityClasses);

		try {
			gtfsReader.setInputLocation(input);
			gtfsReader.run();
			filteredDao = dao;
		} catch (final IOException e) {
			LOG.error("Error while processing GTFS-feed", e);
		}
	}

	public void applyBoundingBoxFilter(final double minlat, final double minlon, final double maxlat, final double maxlon) {
		filteredDao = new BoundingBoxDaoFilter(filteredDao, minlat, minlon, maxlat, maxlon);
	}

	public void applyBoundingPolyFilter(final Shape boundingShape) {
		filteredDao = new BoundingPolyDaoFilter(filteredDao, boundingShape);
	}

	public void applyTimespanFilter(final ServiceDate start, final ServiceDate end) {
		filteredDao = new TimespanDaoFilter(filteredDao, start, end);
	}

	public void applyTimespanFilter(final ServiceDate oneday) {
		filteredDao = new TimespanDaoFilter(filteredDao, oneday);
	}

	public void applyTransportTypeFilter(final int... transportTypes) {
		filteredDao = new TransportTypeDaoFilter(filteredDao, transportTypes);
	}

	public synchronized void write() {
		final GtfsWriter writer = new GtfsWriter();
		writer.setOutputLocation(output);
		try {
			writer.run(filteredDao);
		} catch (final IOException e) {
			LOG.error("Error while writing GTFS-feed", e);
		}
	}

	public static void main(final String[] args) {
		final Options options = createOptions();
		final CommandLineParser parser = new DefaultParser();

		final CommandLine cmd;
		try {
			cmd = parser.parse(options, args);
		} catch (final ParseException e) {
			System.err.println("Parsing failed. Reason: " + e.getMessage());
			showUsage(options);
			return;
		}

		try {
			final String inputFilename = cmd.getOptionValue(INPUT_OPTION);
			final File inputLocation = new File(inputFilename);

			if (!inputLocation.exists()) {
				throw new IOException("Specified input file does not exist!");
			}
			if (!inputLocation.canRead()) {
				throw new IOException("Specified input file can not be read!");
			}

			final String outputFilename;
			if (inputLocation.isFile() && inputFilename.endsWith(GTFS_EXTENSION)) {
				outputFilename = cmd.getOptionValue(OUTPUT_OPTION, inputFilename.replaceAll(Pattern.quote(GTFS_EXTENSION), "_filtered" + GTFS_EXTENSION));
			} else if (inputLocation.isDirectory()) {
				String tmpFilename = inputFilename;
				if (tmpFilename.lastIndexOf(File.separatorChar) != tmpFilename.length() - 1) {
					tmpFilename += File.separatorChar;
				}

				outputFilename = cmd.getOptionValue(OUTPUT_OPTION, tmpFilename + "output" + File.separatorChar);
			} else {
				throw new IOException("Invalid input file specified (neither a GTFS file not a GTFS directory)");
			}

			final File outputLocation = new File(outputFilename);
			if (!outputLocation.exists()) {
				if (outputLocation.isDirectory()) {
					if (!outputLocation.mkdirs()) {
						throw new IOException("Can not create output directory!");
					}
				} else if (outputLocation.isFile()) {
					final File parentFile = outputLocation.getParentFile();
					if (!parentFile.exists() && !parentFile.mkdirs()) {
						throw new IOException("Can not create output file (parent directory creation failed)!");
					}

					if (!outputLocation.getAbsoluteFile().createNewFile()) {
						throw new IOException("Can not create output file!");
					}
				}
			}

			System.out.println("Reading the input GTFS-feed");
			final FilterMain main = new FilterMain(inputLocation, outputLocation);
			main.read();

			if (cmd.hasOption(BOUNDING_BOX_OPTION)) {
				final String[] boundaries = cmd.getOptionValues(BOUNDING_BOX_OPTION);

				// CHECKSTYLE:OFF MagicNumber
				System.out.println("Applying location bounding box filter");
				LOG.info("Applying location filter with restrictions: {},{} --> {}, {}", boundaries[0], boundaries[1], boundaries[2], boundaries[3]);
				final double minLat = Double.parseDouble(boundaries[0]);
				final double minLng = Double.parseDouble(boundaries[1]);
				final double maxLat = Double.parseDouble(boundaries[2]);
				final double maxLng = Double.parseDouble(boundaries[3]);
				// CHECKSTYLE:ON MagicNumber

				main.applyBoundingBoxFilter(minLat, minLng, maxLat, maxLng);
			}

			if (cmd.hasOption(BOUNDING_POLY_OPTION)) {
				final String polyFilename = cmd.getOptionValue(BOUNDING_POLY_OPTION);
				final File polyFile = new File(polyFilename);
				if (!polyFile.exists()) {
					LOG.warn("Referenced poly file ({}) does not exists -> not applying filter", polyFilename);
				} else if (!polyFile.canRead()) {
					LOG.warn("Referenced poly file ({}) can not be read -> not applying filter", polyFilename);
				} else {
					System.out.println("Applying location bounding polygon filter");
					LOG.info("Applying location filter with polyFile: {}", polyFilename);
					if (polyFilename.endsWith(".json")) {
						final GeoJSONReader reader = new GeoJSONReader();
						final List<String> lMbf = Files.readAllLines(Paths.get(polyFilename), StandardCharsets.UTF_8);
						final Geometry geometry = reader.read(lMbf.stream().collect(Collectors.joining()));
						main.applyBoundingPolyFilter(new ShapeWriter().toShape(geometry));
					} else {
						main.applyBoundingPolyFilter(new PolygonFileReader(polyFile).loadPolygon());
					}
				}
			}

			if (cmd.hasOption(TIME_OPTION)) {
				final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				final String[] times = cmd.getOptionValues(TIME_OPTION);
				final Date start = dateFormat.parse(times[0]);

				if (times.length == 1) {
					LOG.info("Applying time filter for one day: {}", new ServiceDate(start));
					main.applyTimespanFilter(new ServiceDate(start));
				} else {
					final Date end = dateFormat.parse(times[1]);
					LOG.info("Applying time filter for timespan: {} --> {}", new ServiceDate(start), new ServiceDate(end));
					main.applyTimespanFilter(new ServiceDate(start), new ServiceDate(end));
				}
			}

			if (cmd.hasOption(TYPE_OPTION)) {
				final String[] types = cmd.getOptionValues(TYPE_OPTION);

				final int[] typeInts = new int[types.length];
				for (int i = 0; i < types.length; i++) {
					typeInts[i] = TRANSPORT_TYPES.get(types[i].toLowerCase(Locale.ENGLISH));
				}

				LOG.info("Applying transport type filters: {}", Arrays.toString(typeInts));
				main.applyTransportTypeFilter(typeInts);
			}

			main.write();
		} catch (IOException | NumberFormatException | java.text.ParseException e) {
			System.err.println("Parsing failed. Reason: " + e.getMessage());
		}
	}

	private static void showUsage(final Options options) {
		final HelpFormatter helpFormatter = new HelpFormatter();
		helpFormatter.printHelp(USAGE, HEADER, options, FOOTER, true);
	}

	private static Options createOptions() {
		// CHECKSTYLE:OFF MagicNumber
		// filters
		final Option boundingBoxOption = Option.builder(BOUNDING_BOX_OPTION)
			.argName("minLat:minLon:maxLat:maxLon")
			.longOpt("bounding-box")
			.desc(DESCRIPTION_OPT_BOUNDING_BOX)
			.numberOfArgs(4)
			.valueSeparator(':')
			.build();

		final Option boundingPolyOption = Option.builder(BOUNDING_POLY_OPTION)
			.argName("region.poly")
			.longOpt("bounding-polygon")
			.desc(DESCRIPTION_OPT_BOUNDING_POLY)
			.numberOfArgs(1)
			.build();

		final Option timespanOption = Option.builder(TIME_OPTION)
			.argName("start:end")
			.longOpt("timespan")
			.desc(DESCRIPTION_OPT_TIME)
			.numberOfArgs(2)
			.valueSeparator(':')
			.build();

		final Option typeOption = Option.builder(TYPE_OPTION)
			.argName("type1,type2")
			.longOpt("type")
			.desc(DESCRIPTION_OPT_TYPE)
			.hasArgs()
			.valueSeparator(',')
			.build();

		// output folder
		final Option outputOption = Option.builder(OUTPUT_OPTION)
			.argName("output")
			.longOpt("output")
			.desc(DESCRIPTION_OPT_OUTPUT)
			.hasArg()
			.build();

		// input GTFS file
		final Option inputOption = Option.builder(INPUT_OPTION)
			.argName("myGtfsFile.zip")
			.longOpt("input")
			.desc(DESCRIPTION_OPT_INPUT)
			.hasArg()
			.required()
			.build();
		// CHECKSTYLE:ON MagicNumber

		final Options options = new Options();
		options.addOption(boundingBoxOption);
		options.addOption(boundingPolyOption);
		options.addOption(timespanOption);
		options.addOption(typeOption);
		options.addOption(outputOption);
		options.addOption(inputOption);

		return options;
	}
}
