package de.krismer.gtfs.predicates;

import com.google.common.base.Predicate;

import org.onebusaway.gtfs.model.ServiceCalendarDate;
import org.onebusaway.gtfs.model.calendar.ServiceDate;

public class TimeSpanDatePredicate implements Predicate<ServiceCalendarDate> {
	private final ServiceDate start;
	private final ServiceDate end;

	public TimeSpanDatePredicate(final ServiceDate start, final ServiceDate end) {
		super();
		this.start = start;
		this.end = end;
	}

	@Override
	public boolean apply(final ServiceCalendarDate scd) {
		if (scd == null) {
			return false;
		}

		return start.compareTo(scd.getDate()) <= 0 && end.compareTo(scd.getDate()) >= 0;
	}

}
