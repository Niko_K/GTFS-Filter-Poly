package de.krismer.gtfs.predicates;

import com.google.common.base.Predicate;

import java.util.Collection;

import org.onebusaway.gtfs.model.Frequency;
import org.onebusaway.gtfs.model.Trip;

public final class FrequencyByTripsPredicate implements Predicate<Frequency> {
	private final Collection<Trip> trips;

	public FrequencyByTripsPredicate(final Collection<Trip> trips) {
		this.trips = trips;
	}

	@Override
	public boolean apply(final Frequency f) {
		if (f == null) {
			return false;
		}

		return trips.contains(f.getTrip());
	}
}
