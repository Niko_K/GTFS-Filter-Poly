package de.krismer.gtfs.predicates;

import com.google.common.base.Predicate;

import org.onebusaway.gtfs.model.ServiceCalendar;
import org.onebusaway.gtfs.model.calendar.ServiceDate;

public class TimeSpanPredicate implements Predicate<ServiceCalendar> {
	private final ServiceDate start;
	private final ServiceDate end;

	public TimeSpanPredicate(final ServiceDate start, final ServiceDate end) {
		super();
		this.start = start;
		this.end = end;
	}

	@Override
	public boolean apply(final ServiceCalendar sc) {
		if (sc == null) {
			return false;
		}

		return start.compareTo(sc.getEndDate()) <= 0 && end.compareTo(sc.getStartDate()) >= 0;
	}

}
