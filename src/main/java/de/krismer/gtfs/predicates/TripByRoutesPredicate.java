package de.krismer.gtfs.predicates;

import com.google.common.base.Predicate;

import java.util.Collection;

import org.onebusaway.gtfs.model.Route;
import org.onebusaway.gtfs.model.Trip;

public final class TripByRoutesPredicate implements Predicate<Trip> {
	private final Collection<Route> routes;

	public TripByRoutesPredicate(final Collection<Route> routes) {
		super();
		this.routes = routes;
	}

	@Override
	public boolean apply(final Trip t) {
		if (t == null) {
			return false;
		}

		return routes.contains(t.getRoute());
	}

}
