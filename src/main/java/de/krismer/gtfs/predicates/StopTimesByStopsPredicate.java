package de.krismer.gtfs.predicates;

import com.google.common.base.Predicate;

import java.util.Collection;

import org.onebusaway.gtfs.model.Stop;
import org.onebusaway.gtfs.model.StopTime;

public final class StopTimesByStopsPredicate implements Predicate<StopTime> {
	private final Collection<Stop> stops;

	public StopTimesByStopsPredicate(final Collection<Stop> stops) {
		super();
		this.stops = stops;
	}

	@Override
	public boolean apply(final StopTime st) {
		if (st == null) {
			return false;
		}

		return stops.contains(st.getStop());
	}

}
