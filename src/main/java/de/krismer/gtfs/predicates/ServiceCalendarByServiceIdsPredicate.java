package de.krismer.gtfs.predicates;

import com.google.common.base.Predicate;

import java.util.Collection;

import org.onebusaway.gtfs.model.AgencyAndId;
import org.onebusaway.gtfs.model.ServiceCalendar;

public final class ServiceCalendarByServiceIdsPredicate implements Predicate<ServiceCalendar> {
	private final Collection<AgencyAndId> serviceIds;

	public ServiceCalendarByServiceIdsPredicate(final Collection<AgencyAndId> serviceIds) {
		this.serviceIds = serviceIds;
	}

	@Override
	public boolean apply(final ServiceCalendar sc) {
		if (sc == null) {
			return false;
		}

		return serviceIds.contains(sc.getServiceId());
	}
}
