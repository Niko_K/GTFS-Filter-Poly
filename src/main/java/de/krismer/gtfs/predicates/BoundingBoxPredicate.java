package de.krismer.gtfs.predicates;

import com.google.common.base.Predicate;

import org.onebusaway.gtfs.model.Stop;

public class BoundingBoxPredicate implements Predicate<Stop> {
	private double minLat;
	private double minLon;
	private double maxLat;
	private double maxLon;


	public BoundingBoxPredicate(final double minLat, final double minLon, final double maxLat, final double maxLon) {
		super();
		this.minLat = minLat;
		this.minLon = minLon;
		this.maxLat = maxLat;
		this.maxLon = maxLon;
	}

	@Override
	public boolean apply(final Stop s) {
		if (s == null) {
			return false;
		}

		return s.getLat() > minLat && s.getLon() > minLon && s.getLat() < maxLat && s.getLon() < maxLon;
	}
}
