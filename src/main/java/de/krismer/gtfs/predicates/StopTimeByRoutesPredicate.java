package de.krismer.gtfs.predicates;

import com.google.common.base.Predicate;

import java.util.Collection;

import org.onebusaway.gtfs.model.Route;
import org.onebusaway.gtfs.model.StopTime;

public final class StopTimeByRoutesPredicate implements Predicate<StopTime> {
	private final Collection<Route> routes;

	public StopTimeByRoutesPredicate(final Collection<Route> routes) {
		super();
		this.routes = routes;
	}

	@Override
	public boolean apply(final StopTime st) {
		if (st == null || st.getTrip() == null) {
			return false;
		}

		return routes.contains(st.getTrip().getRoute());
	}
}
