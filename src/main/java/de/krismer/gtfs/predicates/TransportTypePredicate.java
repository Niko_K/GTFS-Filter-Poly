package de.krismer.gtfs.predicates;

import com.google.common.base.Predicate;
import com.google.common.primitives.Ints;

import java.util.List;

import org.onebusaway.gtfs.model.Route;

public class TransportTypePredicate implements Predicate<Route> {
	private final List<Integer> list;

	public TransportTypePredicate(final int[] transportTypes) {
		this.list = Ints.asList(transportTypes);
	}

	@Override
	public boolean apply(final Route r) {
		if (r == null) {
			return false;
		}

		return list.contains(r.getType());
	}
}
