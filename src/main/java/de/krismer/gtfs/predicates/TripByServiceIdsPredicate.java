package de.krismer.gtfs.predicates;

import com.google.common.base.Predicate;

import java.util.Collection;

import org.onebusaway.gtfs.model.AgencyAndId;
import org.onebusaway.gtfs.model.Trip;

public class TripByServiceIdsPredicate implements Predicate<Trip> {
	private final Collection<AgencyAndId> serviceIds;

	public TripByServiceIdsPredicate(final Collection<AgencyAndId> serviceIds) {
		this.serviceIds = serviceIds;
	}

	@Override
	public boolean apply(final Trip t) {
		if (t == null) {
			return false;
		}

		return serviceIds.contains(t.getServiceId());
	}

}
