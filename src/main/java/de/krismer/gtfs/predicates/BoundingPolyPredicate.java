package de.krismer.gtfs.predicates;

import com.google.common.base.Predicate;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.onebusaway.gtfs.model.Stop;

public class BoundingPolyPredicate implements Predicate<Stop> {
	private final GeometryFactory gFactory;
	private final Geometry polygon;

	public BoundingPolyPredicate(final GeometryFactory gFactory, final Geometry polygon) {
		this.gFactory = gFactory;
		this.polygon = polygon;
	}

	public BoundingPolyPredicate(final Geometry polygon) {
		this(new GeometryFactory(), polygon);
	}

	@Override
	public boolean apply(final Stop s) {
		if (s == null) {
			return false;
		}

		final Coordinate sCoord = new Coordinate(s.getLon(), s.getLat());
		final Point sPoint = gFactory.createPoint(sCoord);
		return sPoint.within(polygon);
	}

}
