package de.krismer.gtfs.predicates;

import com.google.common.base.Predicate;

import java.util.Collection;

import org.onebusaway.gtfs.model.AgencyAndId;
import org.onebusaway.gtfs.model.ServiceCalendarDate;

public final class ServiceCalendarDateByServiceIdsPredicate implements Predicate<ServiceCalendarDate> {
	private final Collection<AgencyAndId> serviceIds;

	public ServiceCalendarDateByServiceIdsPredicate(final Collection<AgencyAndId> serviceIds) {
		this.serviceIds = serviceIds;
	}

	@Override
	public boolean apply(final ServiceCalendarDate scd) {
		if (scd == null) {
			return false;
		}

		return serviceIds.contains(scd.getServiceId());
	}
}
