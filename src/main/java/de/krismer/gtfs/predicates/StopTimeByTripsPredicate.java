package de.krismer.gtfs.predicates;

import com.google.common.base.Predicate;

import java.util.Collection;

import org.onebusaway.gtfs.model.StopTime;
import org.onebusaway.gtfs.model.Trip;

public class StopTimeByTripsPredicate implements Predicate<StopTime> {
	private final Collection<Trip> trips;

	public StopTimeByTripsPredicate(final Collection<Trip> trips) {
		this.trips = trips;
	}

	@Override
	public boolean apply(final StopTime st) {
		if (st == null) {
			return false;
		}

		return trips.contains(st.getTrip());
	}

}
