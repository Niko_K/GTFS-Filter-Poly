package de.krismer.gtfs.predicates;

import com.google.common.base.Predicate;

import java.util.Collection;

import org.onebusaway.gtfs.model.AgencyAndId;
import org.onebusaway.gtfs.model.ShapePoint;

public final class ShapePointsByShapeIdsPredicate implements Predicate<ShapePoint> {
	private final Collection<AgencyAndId> shapeIds;

	public ShapePointsByShapeIdsPredicate(final Collection<AgencyAndId> shapeIds) {
		this.shapeIds = shapeIds;
	}

	@Override
	public boolean apply(final ShapePoint sp) {
		if (sp == null) {
			return false;
		}

		return shapeIds.contains(sp.getShapeId());
	}
}
