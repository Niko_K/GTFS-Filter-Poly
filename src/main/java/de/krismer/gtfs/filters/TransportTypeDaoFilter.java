package de.krismer.gtfs.filters;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.onebusaway.gtfs.model.AgencyAndId;
import org.onebusaway.gtfs.model.Frequency;
import org.onebusaway.gtfs.model.Route;
import org.onebusaway.gtfs.model.ServiceCalendar;
import org.onebusaway.gtfs.model.ServiceCalendarDate;
import org.onebusaway.gtfs.model.ShapePoint;
import org.onebusaway.gtfs.model.Stop;
import org.onebusaway.gtfs.model.StopTime;
import org.onebusaway.gtfs.model.Trip;
import org.onebusaway.gtfs.services.GtfsDao;

import de.krismer.gtfs.predicates.FrequencyByTripsPredicate;
import de.krismer.gtfs.predicates.ServiceCalendarByServiceIdsPredicate;
import de.krismer.gtfs.predicates.ServiceCalendarDateByServiceIdsPredicate;
import de.krismer.gtfs.predicates.ShapePointsByShapeIdsPredicate;
import de.krismer.gtfs.predicates.StopTimeByRoutesPredicate;
import de.krismer.gtfs.predicates.TransportTypePredicate;
import de.krismer.gtfs.predicates.TripByRoutesPredicate;
import de.krismer.gtfs.transformers.StopTimeToStopFunction;
import de.krismer.gtfs.transformers.TripToServiceIdFunction;
import de.krismer.gtfs.transformers.TripToShapeIdFunction;

public class TransportTypeDaoFilter extends GtfsDaoFilter {
	private static final Logger LOG = LogManager.getLogger(TransportTypeDaoFilter.class);

	private final Set<Route> routes;
	private final Set<Stop> stops;
	private final Collection<Trip> trips;
	private final Collection<StopTime> stoptimes;

	private final Set<AgencyAndId> serviceIds;
	private final Set<AgencyAndId> shapeIds;

	public TransportTypeDaoFilter(final GtfsDao dao, final int[] transportTypes) {
		super(dao);

		Preconditions.checkArgument(transportTypes.length > 0);

		final Predicate<Route> p = new TransportTypePredicate(transportTypes);
		routes = new HashSet<>();
		routes.addAll(Collections2.filter(dao.getAllRoutes(), p));
		LOG.info("Filtered down from {} to {} routes", input.getAllRoutes().size(), routes.size());

		trips = Collections2.filter(super.getAllTrips(), new TripByRoutesPredicate(routes));
		LOG.info("Filtered down from {} to {} trips", super.getAllTrips().size(), trips.size());

		stoptimes = Collections2.filter(super.getAllStopTimes(), new StopTimeByRoutesPredicate(routes));
		LOG.info("Filtered down from {} to {} stoptimes", input.getAllStopTimes().size(), stoptimes.size());

		stops = new HashSet<>();
		stops.addAll(Collections2.transform(stoptimes, new StopTimeToStopFunction()));
		LOG.info("Filtered down from {} to {} stops", input.getAllStops().size(), stops.size());

		serviceIds = new HashSet<>();
		serviceIds.addAll(Collections2.transform(trips, new TripToServiceIdFunction()));
		LOG.info("Filtered down to {} serviceIds", serviceIds.size());

		shapeIds = new HashSet<>();
		shapeIds.addAll(Collections2.transform(trips, new TripToShapeIdFunction()));
		LOG.info("Filtered down to {} shapeIds", shapeIds.size());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see be.ugent.intec.gtfsfilter.GtfsDaoFilter#getAllRoutes()
	 */
	@Override
	public Collection<Route> getAllRoutes() {
		return routes;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see be.ugent.intec.gtfsfilter.GtfsDaoFilter#getAllTrips()
	 */
	@Override
	public synchronized Collection<Trip> getAllTrips() {
		return trips;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see be.ugent.intec.gtfsfilter.GtfsDaoFilter#getAllStopTimes()
	 */
	@Override
	public synchronized Collection<StopTime> getAllStopTimes() {
		return stoptimes;

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see be.ugent.intec.gtfsfilter.GtfsDaoFilter#getAllStops()
	 */
	@Override
	public Collection<Stop> getAllStops() {
		return stops;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see be.ugent.intec.gtfsfilter.GtfsDaoFilter#getAllCalendars()
	 */
	@Override
	public Collection<ServiceCalendar> getAllCalendars() {
		return Collections2.filter(super.getAllCalendars(), new ServiceCalendarByServiceIdsPredicate(serviceIds));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see be.ugent.intec.gtfsfilter.GtfsDaoFilter#getAllCalendarDates()
	 */
	@Override
	public Collection<ServiceCalendarDate> getAllCalendarDates() {
		return Collections2.filter(super.getAllCalendarDates(),
				new ServiceCalendarDateByServiceIdsPredicate(serviceIds));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see be.ugent.intec.gtfsfilter.GtfsDaoFilter#getAllFrequencies()
	 */
	@Override
	public Collection<Frequency> getAllFrequencies() {
		return Collections2.filter(super.getAllFrequencies(), new FrequencyByTripsPredicate(trips));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see be.ugent.intec.gtfsfilter.GtfsDaoFilter#getAllShapePoints()
	 */
	@Override
	public Collection<ShapePoint> getAllShapePoints() {
		return Collections2.filter(super.getAllShapePoints(), new ShapePointsByShapeIdsPredicate(shapeIds));
	}

}
