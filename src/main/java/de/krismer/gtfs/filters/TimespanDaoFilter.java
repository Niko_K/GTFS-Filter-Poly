package de.krismer.gtfs.filters;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.onebusaway.gtfs.model.AgencyAndId;
import org.onebusaway.gtfs.model.Frequency;
import org.onebusaway.gtfs.model.Route;
import org.onebusaway.gtfs.model.ServiceCalendar;
import org.onebusaway.gtfs.model.ServiceCalendarDate;
import org.onebusaway.gtfs.model.ShapePoint;
import org.onebusaway.gtfs.model.Stop;
import org.onebusaway.gtfs.model.StopTime;
import org.onebusaway.gtfs.model.Trip;
import org.onebusaway.gtfs.model.calendar.ServiceDate;
import org.onebusaway.gtfs.services.GtfsDao;

import de.krismer.gtfs.predicates.FrequencyByTripsPredicate;
import de.krismer.gtfs.predicates.ShapePointsByShapeIdsPredicate;
import de.krismer.gtfs.predicates.StopTimeByRoutesPredicate;
import de.krismer.gtfs.predicates.StopTimeByTripsPredicate;
import de.krismer.gtfs.predicates.TimeSpanDatePredicate;
import de.krismer.gtfs.predicates.TimeSpanPredicate;
import de.krismer.gtfs.predicates.TripByServiceIdsPredicate;
import de.krismer.gtfs.transformers.ServiceCalendarDateToServiceIdFunction;
import de.krismer.gtfs.transformers.ServiceCalendarToServiceIdFunction;
import de.krismer.gtfs.transformers.StopTimeToStopFunction;
import de.krismer.gtfs.transformers.TripToRouteFunction;
import de.krismer.gtfs.transformers.TripToShapeIdFunction;

public class TimespanDaoFilter extends GtfsDaoFilter {
	private static final Logger LOG = LogManager.getLogger(TimespanDaoFilter.class);

	private List<ServiceCalendar> calendars;
	private Collection<ServiceCalendarDate> calendarDates;
	private Set<AgencyAndId> serviceIds;
	private Set<AgencyAndId> shapeIds;
	private Set<Trip> trips; // use set instead of collection to optimize
								// stoptimes-filtering
	private Set<Route> routes;
	private Collection<StopTime> stoptimes;
	private Set<Stop> stops;

	public TimespanDaoFilter(final GtfsDao input, final ServiceDate oneDay) {
		this(input, oneDay, oneDay);
	}

	public TimespanDaoFilter(final GtfsDao input, final ServiceDate start, final ServiceDate end) {
		super(input);

		// filter calendars and calendardates
		final Predicate<ServiceCalendar> p = new TimeSpanPredicate(start, end);
		calendars = new ArrayList<>();
		calendars.addAll(Collections2.filter(input.getAllCalendars(), p));

		// change the calendar begin- and enddates for consistency
		for (final ServiceCalendar sc : calendars) {
			if (start.compareTo(sc.getStartDate()) > 1) {
				sc.setStartDate(start);
			}
			if (end.compareTo(sc.getEndDate()) < 1) {
				sc.setEndDate(end);
			}
		}
		LOG.info("Filtered down to {} calendars", calendars.size());

		final Predicate<ServiceCalendarDate> pDate = new TimeSpanDatePredicate(start, end);
		calendarDates = Collections2.filter(input.getAllCalendarDates(), pDate);
		LOG.info("Filtered down to {} calendardates", calendarDates.size());

		serviceIds = new HashSet<>();
		serviceIds.addAll(Collections2.transform(calendars, new ServiceCalendarToServiceIdFunction()));
		serviceIds.addAll(Collections2.transform(calendarDates, new ServiceCalendarDateToServiceIdFunction()));
		LOG.info("Filtered down to {} serviceIds", serviceIds.size());

		trips = new HashSet<>();
		trips.addAll(Collections2.filter(input.getAllTrips(), new TripByServiceIdsPredicate(serviceIds)));
		LOG.info("Filtered down from {} to {} trips", super.getAllTrips().size(), trips.size());

		routes = new HashSet<>();
		routes.addAll(Collections2.transform(trips, new TripToRouteFunction()));
		LOG.info("Filtered down from {} to {} routes", input.getAllRoutes().size(), routes.size());

		stoptimes = Collections2.filter(input.getAllStopTimes(), new StopTimeByRoutesPredicate(routes));
		LOG.info("Filtered down from {} to {} stoptimes after 1st pass", input.getAllStopTimes().size(), stoptimes.size());
		stoptimes = Collections2.filter(stoptimes, new StopTimeByTripsPredicate(trips));
		LOG.info("Filtered down to {} stoptimes after 2nd pass", stoptimes.size());

		stops = new HashSet<>();
		stops.addAll(Collections2.transform(stoptimes, new StopTimeToStopFunction()));
		LOG.info("Filtered down from {} to {} stops", input.getAllStops().size(), stops.size());

		shapeIds = new HashSet<>();
		shapeIds.addAll(Collections2.transform(trips, new TripToShapeIdFunction()));
		LOG.info("Filtered down to {} shapeIds", shapeIds.size());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see be.ugent.intec.gtfsfilter.GtfsDaoFilter#getAllCalendars()
	 */
	@Override
	public Collection<ServiceCalendar> getAllCalendars() {
		return calendars;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see be.ugent.intec.gtfsfilter.GtfsDaoFilter#getAllCalendarDates()
	 */
	@Override
	public Collection<ServiceCalendarDate> getAllCalendarDates() {
		return calendarDates;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see be.ugent.intec.gtfsfilter.GtfsDaoFilter#getAllFrequencies()
	 */
	@Override
	public Collection<Frequency> getAllFrequencies() {
		return Collections2.filter(super.getAllFrequencies(), new FrequencyByTripsPredicate(trips));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see be.ugent.intec.gtfsfilter.GtfsDaoFilter#getAllRoutes()
	 */
	@Override
	public Collection<Route> getAllRoutes() {
		return routes;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see be.ugent.intec.gtfsfilter.GtfsDaoFilter#getAllShapePoints()
	 */
	@Override
	public Collection<ShapePoint> getAllShapePoints() {
		return Collections2.filter(super.getAllShapePoints(), new ShapePointsByShapeIdsPredicate(shapeIds));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see be.ugent.intec.gtfsfilter.GtfsDaoFilter#getAllStops()
	 */
	@Override
	public Collection<Stop> getAllStops() {
		return stops;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see be.ugent.intec.gtfsfilter.GtfsDaoFilter#getAllStopTimes()
	 */
	@Override
	public Collection<StopTime> getAllStopTimes() {
		return stoptimes;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see be.ugent.intec.gtfsfilter.GtfsDaoFilter#getAllTrips()
	 */
	@Override
	public Collection<Trip> getAllTrips() {
		return trips;
	}

}
