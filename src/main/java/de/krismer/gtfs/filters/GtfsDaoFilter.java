package de.krismer.gtfs.filters;

import java.io.Serializable;
import java.util.Collection;

import org.onebusaway.gtfs.model.Agency;
import org.onebusaway.gtfs.model.AgencyAndId;
import org.onebusaway.gtfs.model.FareAttribute;
import org.onebusaway.gtfs.model.FareRule;
import org.onebusaway.gtfs.model.FeedInfo;
import org.onebusaway.gtfs.model.Frequency;
import org.onebusaway.gtfs.model.Pathway;
import org.onebusaway.gtfs.model.Route;
import org.onebusaway.gtfs.model.ServiceCalendar;
import org.onebusaway.gtfs.model.ServiceCalendarDate;
import org.onebusaway.gtfs.model.ShapePoint;
import org.onebusaway.gtfs.model.Stop;
import org.onebusaway.gtfs.model.StopTime;
import org.onebusaway.gtfs.model.Transfer;
import org.onebusaway.gtfs.model.Trip;
import org.onebusaway.gtfs.services.GtfsDao;

public abstract class GtfsDaoFilter implements GtfsDao {

	protected final GtfsDao input;

	protected GtfsDaoFilter(final GtfsDao input) {
		this.input = input;
	}

	@Override
	public Agency getAgencyForId(final String id) {
		return input.getAgencyForId(id);
	}

	@Override
	public Collection<Agency> getAllAgencies() {
		return input.getAllAgencies();
	}

	@Override
	public Collection<ServiceCalendarDate> getAllCalendarDates() {
		return input.getAllCalendarDates();
	}

	@Override
	public Collection<ServiceCalendar> getAllCalendars() {
		return input.getAllCalendars();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> Collection<T> getAllEntitiesForType(final Class<T> type) {
		if (type == Agency.class) {
			return (Collection<T>) getAllAgencies();
		}
		if (type == ShapePoint.class) {
			return (Collection<T>) getAllShapePoints();
		}
		if (type == Route.class) {
			return (Collection<T>) getAllRoutes();
		}
		if (type == Stop.class) {
			return (Collection<T>) getAllStops();
		}
		if (type == Trip.class) {
			return (Collection<T>) getAllTrips();
		}
		if (type == StopTime.class) {
			return (Collection<T>) getAllStopTimes();
		}
		if (type == ServiceCalendar.class) {
			return (Collection<T>) getAllCalendars();
		}
		if (type == ServiceCalendarDate.class) {
			return (Collection<T>) getAllCalendarDates();
		}
		if (type == FareAttribute.class) {
			return (Collection<T>) getAllFareAttributes();
		}
		if (type == FareRule.class) {
			return (Collection<T>) getAllFareRules();
		}
		if (type == FeedInfo.class) {
			return (Collection<T>) getAllFeedInfos();
		}
		if (type == Frequency.class) {
			return (Collection<T>) getAllFrequencies();
		}
		if (type == Pathway.class) {
			return (Collection<T>) getAllPathways();
		}
		if (type == Transfer.class) {
			return (Collection<T>) getAllTransfers();
		}

		throw new IllegalArgumentException("Unknown class");
	}

	@Override
	public Collection<FareAttribute> getAllFareAttributes() {
		return input.getAllFareAttributes();
	}

	@Override
	public Collection<FareRule> getAllFareRules() {
		return input.getAllFareRules();
	}

	@Override
	public Collection<FeedInfo> getAllFeedInfos() {
		return input.getAllFeedInfos();
	}

	@Override
	public Collection<Frequency> getAllFrequencies() {
		return input.getAllFrequencies();
	}

	@Override
	public Collection<Pathway> getAllPathways() {
		return input.getAllPathways();
	}

	@Override
	public Collection<Route> getAllRoutes() {
		return input.getAllRoutes();
	}

	@Override
	public Collection<ShapePoint> getAllShapePoints() {
		return input.getAllShapePoints();
	}

	@Override
	public Collection<Stop> getAllStops() {
		return input.getAllStops();
	}

	@Override
	public Collection<StopTime> getAllStopTimes() {
		return input.getAllStopTimes();
	}

	@Override
	public Collection<Transfer> getAllTransfers() {
		return input.getAllTransfers();
	}

	@Override
	public Collection<Trip> getAllTrips() {
		return input.getAllTrips();
	}

	@Override
	public ServiceCalendarDate getCalendarDateForId(final int id) {
		return input.getCalendarDateForId(id);
	}

	@Override
	public ServiceCalendar getCalendarForId(final int id) {
		return input.getCalendarForId(id);
	}

	@Override
	public <T> T getEntityForId(final Class<T> type, final Serializable id) {
		return input.getEntityForId(type, id);
	}

	@Override
	public FareAttribute getFareAttributeForId(final AgencyAndId id) {
		return input.getFareAttributeForId(id);
	}

	@Override
	public FareRule getFareRuleForId(final int id) {
		return input.getFareRuleForId(id);
	}

	@Override
	public FeedInfo getFeedInfoForId(final int id) {
		return input.getFeedInfoForId(id);
	}

	@Override
	public Frequency getFrequencyForId(final int id) {
		return input.getFrequencyForId(id);
	}

	@Override
	public Pathway getPathwayForId(final AgencyAndId id) {
		return input.getPathwayForId(id);
	}

	@Override
	public Route getRouteForId(final AgencyAndId id) {
		return input.getRouteForId(id);
	}

	@Override
	public ShapePoint getShapePointForId(final int id) {
		return input.getShapePointForId(id);
	}

	@Override
	public Stop getStopForId(final AgencyAndId id) {
		return input.getStopForId(id);
	}

	@Override
	public StopTime getStopTimeForId(final int id) {
		return input.getStopTimeForId(id);
	}

	@Override
	public Transfer getTransferForId(final int id) {
		return input.getTransferForId(id);
	}

	@Override
	public Trip getTripForId(final AgencyAndId id) {
		return input.getTripForId(id);
	}

}
