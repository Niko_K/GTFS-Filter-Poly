GTFS-Filter-Poly
================

Filters GTFS-feeds to create smaller samples

Compiling
---------
To get an executable jar use:

	./gradlew shadowJar


Usage
-----

Call

	./gradlew run

to get some usage information.


License
-------
GTFS-Filter-Poly is distributed under the GNU General Public License v3.
Please refer to LICENSE.md for more information.


Copyright
---------
GTFS-Filter-Poly was forked from gtfs-filter on 08/18/2015
The original project is located on github under:
https://github.com/twalcari/gtfs-filter

(c) 2011-2015 Thijs Walcarius <Thijs.Walcarius@UGent.be>
(c) 2015-2018 Nikolaus Krismer <niko@krismer.de>

