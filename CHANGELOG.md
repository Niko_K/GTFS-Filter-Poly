Upcoming version:
-----------------
  - upgrading scmversion plugin (fixes compatibility warnings for gradle 5.x) (Nikolaus Krismer)
  - renaming gradle daily task to stage (Nikolaus Krismer)
  - upgrading to gradle 4.10.2 (Nikolaus Krismer)

Version v0.9.2:
---------------
  - added javadoc option check (Nikolaus Krismer)
  - added html5 option to javadoc generation (Nikolaus Krismer)
  - removed duplicated code (Nikolaus Krismer)
  - upgrading gradle to version 4.10 (Nikolaus Krismer)
  - added functionality to use boundingPolygon either in poly format or geojson format (Nikolaus Krismer)
  - upgrading JTS to latest version 1.15 (Nikolaus Krismer)
  - removed gradle workaround (Nikolaus Krismer)
  - fixed filter naming (Nikolaus Krismer)
  - adding eclipse filter for test-results (Nikolaus Krismer)
  - updating gradle eclipse plugin configuration (Nikolaus Krismer)
  - updating changelog (Nikolaus Krismer)
  - chaning namespace to "de.krismer" (Nikolaus Krismer)
  - using latest version of jacoco (Nikolaus Krismer)
  - upgrading to gradle 4.9 (Nikolaus Krismer)
  - Updating copyright information (new e-mail) (Nikolaus Krismer)
  - adding doctype to log4j2 configuration files (Nikolaus Krismer)
  - gradle version upgrade (to version 4.8.1) and eased gradle.properties usage (Nikolaus Krismer)
  - changes to fit gradle 4.8 (Nikolaus Krismer)
  - upgrading gradle wrapper to version 4.8 (Nikolaus Krismer)
  - gradle version update; updated README (Nikolaus Krismer)
  - using nexus.krismer.de repo now (Nikolaus Krismer)
  - upgrading to gradle 4.6 (Nikolaus Krismer)

Version v0.9.1:
---------------
  - updating gradle (to version 4.2) (Nikolaus Krismer)
  - upgrading gradle plugin shadowjar (Nikolaus Krismer)
  - updating to gradle 4.1 (Nikolaus Krismer)

Version v0.9.0:
---------------
  - Updating to gradle 4.0 (Nikolaus Krismer)
  - Adding pattern.quote for safety (Nikolaus Krismer)
  - Updating gradle (to version 3.5) (Nikolaus Krismer)
  - Updating jacoco to latest version (v0.7.9) (Nikolaus Krismer)
  - updating gradle wrapper (to version 3.4.1) (Nikolaus Krismer)
  - updating gradle wrapper (to version 3.4) (Nikolaus Krismer)
  - updating gradle version (to version 3.3) (Nikolaus Krismer)

Version v0.8.1:
---------------
  - updating gradle to version 3.2.1 (Nikolaus Krismer)
  - updating gradle to version 3.2 (Nikolaus Krismer)

Version v0.8.0:
---------------
  - update to latest version of gradle shadowJar plugin (Nikolaus Krismer)

Version v0.6.0:
---------------
  - added entity class filter (to work with berlin gtfs file) (Nikolaus Krismer)
  - jacoco and scmversion udpate (Nikolaus Krismer)
  - Adding changelog commit-ability (Nikolaus Krismer)
  - fixing invalid clean task definition (Nikolaus Krismer)
  - adding removal of CHANGELOG to clean task (Nikolaus Krismer)
  - gradle scmversion plugin update (to version 0.6.4) (Nikolaus Krismer)
  - fixed problems with scmversion in build.gradle (Nikolaus Krismer)
  - adding missing import statement in build.gradle file (Nikolaus Krismer)
  - removed hard-coded version from gradle (is set using scmversion plugin) (Nikolaus Krismer)
  - added changelog task (Nikolaus Krismer)
  - improved output string a bit (Nikolaus Krismer)

Version v0.2.3:
---------------
  - added missing scmversion part in gradle build file (Nikolaus Krismer)
  - eased project versioning by using a new gradle plugin (Nikolaus Krismer)
  - inceasing version number (after releasing version v0.2.2) (Nikolaus Krismer)

Version v0.2.2:
---------------
  - fixing a problem with the output file creation (Nikolaus Krismer)
  - increasing version number (after releasing version 0.2.1) (Nikolaus Krismer)

Version v0.2.1:
---------------
  - throwing exception if output file/folder creation unsuccessful (Nikolaus Krismer)
  - renamed main class (Nikolaus Krismer)
  - some package re-structuring (to prevent multiple predicate creations) (Nikolaus Krismer)
  - fixed some findbugs warnings (Nikolaus Krismer)
  - fixed some minor checkstyle issues (Nikolaus Krismer)
  - increased version number (after releasing version 0.2.0) (Nikolaus Krismer)

Version V0.9.1:
---------------
  - updating gradle (to version 4.2) (Nikolaus Krismer)
  - upgrading gradle plugin shadowjar (Nikolaus Krismer)
  - updating to gradle 4.1 (Nikolaus Krismer)
  - Updating to gradle 4.0 (Nikolaus Krismer)
  - Adding pattern.quote for safety (Nikolaus Krismer)
  - Updating gradle (to version 3.5) (Nikolaus Krismer)
  - Updating jacoco to latest version (v0.7.9) (Nikolaus Krismer)
  - updating gradle wrapper (to version 3.4.1) (Nikolaus Krismer)
  - updating gradle wrapper (to version 3.4) (Nikolaus Krismer)
  - updating gradle version (to version 3.3) (Nikolaus Krismer)
  - updating gradle to version 3.2.1 (Nikolaus Krismer)
  - updating gradle to version 3.2 (Nikolaus Krismer)
  - update to latest version of gradle shadowJar plugin (Nikolaus Krismer)
  - added entity class filter (to work with berlin gtfs file) (Nikolaus Krismer)
  - jacoco and scmversion udpate (Nikolaus Krismer)
  - Adding changelog commit-ability (Nikolaus Krismer)
  - fixing invalid clean task definition (Nikolaus Krismer)
  - adding removal of CHANGELOG to clean task (Nikolaus Krismer)
  - gradle scmversion plugin update (to version 0.6.4) (Nikolaus Krismer)
  - fixed problems with scmversion in build.gradle (Nikolaus Krismer)
  - adding missing import statement in build.gradle file (Nikolaus Krismer)
  - removed hard-coded version from gradle (is set using scmversion plugin) (Nikolaus Krismer)
  - added changelog task (Nikolaus Krismer)
  - improved output string a bit (Nikolaus Krismer)
  - added missing scmversion part in gradle build file (Nikolaus Krismer)
  - eased project versioning by using a new gradle plugin (Nikolaus Krismer)
  - inceasing version number (after releasing version v0.2.2) (Nikolaus Krismer)
  - fixing a problem with the output file creation (Nikolaus Krismer)
  - increasing version number (after releasing version 0.2.1) (Nikolaus Krismer)
  - throwing exception if output file/folder creation unsuccessful (Nikolaus Krismer)
  - renamed main class (Nikolaus Krismer)
  - some package re-structuring (to prevent multiple predicate creations) (Nikolaus Krismer)
  - fixed some findbugs warnings (Nikolaus Krismer)
  - fixed some minor checkstyle issues (Nikolaus Krismer)
  - increased version number (after releasing version 0.2.0) (Nikolaus Krismer)
  - fixed version number (Nikolaus Krismer)
  - some preparations for release publishing (Nikolaus Krismer)
  - added javadoc and sourceJar (Nikolaus Krismer)
  - removed duplicate definitions (Nikolaus Krismer)
  - preparations for maven publish (Nikolaus Krismer)
  - minor improvements in usage description (Nikolaus Krismer)
  - Moving project from github to dbis-git. Allowing for filtering by bounding-polygons. (Nikolaus Krismer)
  - Fixed TransportTypeFilter-bug (Thijs Walcarius)
  - Releasing version 0.1 (Thijs Walcarius)
  - resolved conflict (Thijs Walcarius)
  - Added license (Thijs Walcarius)
  - Update README.md (twalcari)
  - Updated readme (Thijs Walcarius)
  - Updated readme (Thijs Walcarius)
  - Finished CLI Cleaned up logging Changed some filenames (Thijs Walcarius)
  - Untested CLI implementation (Thijs Walcarius)
  - Merge branch 'master' of github.com:twalcari/gtfs-filter (Thijs Walcarius)
  - Separation of Predicate- and Function-classes in separate files, added TimespanDAOFilter and LocationDaoFilter and made TramDaoFilter more general and renamed it to TransportTypeDaoFilter (Thijs Walcarius)
  - Separation of Predicate- and Function-classes in separate files, added TimespanDAOFilter and LocationDaoFilter and made TramDaoFilter more general and renamed it to TransportTypeDaoFilter (Thijs Walcarius)
  - Adding readme (Thijs Walcarius)
  - Initial commit (Thijs Walcarius)

